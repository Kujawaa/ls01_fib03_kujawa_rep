package de.danielkujawa.fahrkartenautomat;

import java.util.Scanner;

public class Fahrkartenautomat {

	public static void main(String[] args) {
		while (true) {
			Scanner tastatur = new Scanner(System.in);
	
			double zuZahlenderBetrag = fahrkartenbestellungErfassen();
			double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			double eingeworfeneM�nze;
			double r�ckgabebetrag;
			int AnzahlderTickets;
			
	
			// Fahrscheinausgabe
	
			fahrkartenAusgeben();
	
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

		}
	}
	public static double fahrkartenbestellungErfassen() {

		Scanner erfassung = new Scanner(System.in);

		double zuZahlenderBetrag=0.0;
		int AnzahlderTickets;
		short Wunschfahrkarte;

		String [] Fahrkartenbezeichnung = {"EinzelfahrscheinRegeltarifAB(0)","TageskarteRegeltarifAB(1)","KleingruppenTageskarteRegeltarifAB(2)"};	
				//im Array festelgen welche Tickets es geben soll
		double [] Fahrkartenpreise = {2.90,8.60,23.50};
				//im Array festlegen welche Preise es geben soll
		System.out.println("W�hlen sie ihre Wunschfahrkarte aus:");
		
		for (int i=0;i<Fahrkartenbezeichnung.length;i++) {				//ganzes Array Fahrkartenbezeichnung ausgeben
			System.out.println(Fahrkartenbezeichnung[i]);
			
		}
		
		zuZahlenderBetrag=Fahrkartenpreise [erfassung.nextInt()];		//zuZahlenderBetrag festlegen => mit dem Array Fahrkartenpreise	
		
		System.out.print("Anzahl der Tickets: ");
		AnzahlderTickets = erfassung.nextInt();
		
		
		
		if (AnzahlderTickets > 10) { // Tickets bei Bestellung auf 10 beschr�nken
			System.out.println("Fehler! es k�nnen maximal 10 tickets gekauft werden, es wird nun nurnoch 1 bestellt!");
			AnzahlderTickets = 1;
		}
		if (zuZahlenderBetrag < 1) {
			System.out.println("Fehler! Keine negativen Ticketpreise!,Preis betr�gt nun 2 Euro f�r 1 Ticket");
			AnzahlderTickets = 1;
			zuZahlenderBetrag = 2;
		}
		while (AnzahlderTickets < 1 ) {
			
			
			System.out.print("Anzahl der Tickets ben�tigt g�ltigen Wert: ");
			AnzahlderTickets = erfassung.nextInt();
			
		}
		
		zuZahlenderBetrag *= AnzahlderTickets;
		return zuZahlenderBetrag; // wird zur�ckgegeben in MainMethode

	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner bezahlen = new Scanner(System.in);

		double eingezahlterGesamtbetrag;
		double eingeworfeneM�nze = 0;

		eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = bezahlen.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
			

		}
		return eingeworfeneM�nze;
	}

	public static void fahrkartenAusgeben() {

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");

		}
		System.out.println("\n\n");
	}

	public static double rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {

		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f Euro\n", (Math.round(r�ckgabebetrag * 100) / 100.0));
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				muenzeAusgeben(2, "EURO");
				r�ckgabebetrag = Math.round((r�ckgabebetrag - 2.0) * 100) / 100.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				muenzeAusgeben(1, "EURO");
				r�ckgabebetrag = Math.round((r�ckgabebetrag - 1.0) * 100) / 100.0;
			}
			while (r�ckgabebetrag >= 0.50) // 50 CENT-M�nzen
			{
				muenzeAusgeben(50, "Cent");
				r�ckgabebetrag = Math.round((r�ckgabebetrag - 0.50) * 100) / 100.0;
			}
			while (r�ckgabebetrag >= 0.20) // 20 CENT-M�nzen
			{
				muenzeAusgeben(20, "Cent");
				r�ckgabebetrag = Math.round((r�ckgabebetrag - 0.20) * 100) / 100.0;
			}
			while (r�ckgabebetrag >= 0.10) // 10 CENT-M�nzen
			{
				muenzeAusgeben(10, "Cent");
				r�ckgabebetrag = Math.round((r�ckgabebetrag - 0.10) * 100) / 100.0;
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				muenzeAusgeben(5, "Cent");
				r�ckgabebetrag = Math.round((r�ckgabebetrag - 0.05) * 100) / 100.0;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
		System.out.println(" ");
		return r�ckgabebetrag;
	}

	public static void warte(int millisekunde) { // Methode warte definiert die Zeit die der Automat bruacht um zu
													// drucken

		try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);// " " damit ein leerzeichen zwischen betrag und einhiet ist

	}

}
